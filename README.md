Transcribe~ uses pre-loaded Pitch Templates (learned offline) to detect the presence of multiple pitches using those templates in real-time.

This version of Transcribe~ uses templates pre-learned on Piano. Using other instruments ideally requires updating the template.

Loading templates is now available as an option. We will release code for learning pitch templates from instrument databases in the future.

It is based on R&D work in IRCAM's MuTant team by Arshia Cont and Arnaud Dessein.